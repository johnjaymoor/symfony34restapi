<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerTxidcxu\appDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerTxidcxu/appDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerTxidcxu.legacy');

    return;
}

if (!\class_exists(appDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerTxidcxu\appDevDebugProjectContainer::class, appDevDebugProjectContainer::class, false);
}

return new \ContainerTxidcxu\appDevDebugProjectContainer([
    'container.build_hash' => 'Txidcxu',
    'container.build_id' => '815d4498',
    'container.build_time' => 1554558524,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerTxidcxu');
